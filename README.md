# Inventory



## Getting started

## How To Start Clone Project

1. Run **```git clone link-to-git-repo```** (change it with actual link), on your local pc and run **```cd project_name```** (change it with actual directory name) to move in the directory of project.
1. Run **```composer install```** <br>This will install all of the project dependencies. It will installs Laravel itself, among other necessary packages to get started. It will checks the ```composer.json``` file which is submitted to the git repo and lists all of the composer (PHP) packages that your repo requires. 
1. Run **```cp .env.example .env```** <br>```.env``` files are not generally committed to source control for security reasons. But there is a ```.env.example``` which is a template of the ```.env``` file that the project expects us to have. So we will make a copy of the ```.env.example``` file and create a ```.env``` file that we can start to fill out to do things like database configuration in the next few steps.
1. Run **```php artisan key:generate```** <br>
Laravel requires you to have an app encryption key which is generally randomly generated and stored in your ```.env``` file. The app will use this encryption key to encode various elements of your application from cookies to password hashes and more.
1. Create an empty database and in the ```.env``` file fill in the ```DB_HOST```, ```DB_PORT```, ```DB_DATABASE```, ```DB_USERNAME```, and ```DB_PASSWORD``` options to match the credentials of the database you just created.
1. Run **```php artisan migrate```**<br>Once your credentials are in the ```.env``` file, now you can migrate your database. You can also check your database to make sure everything migrated the way you expected.
1. Run **```php artisan db:seed```**
This process will fill the table with initial data row. It can be use like first config to make project can be run at first time. 
1. If you want to setup a ```virtual host``` in your pc to run this program, make sure you follow the correct way to do that. Also consider what type of web server you are using (apache or nginx or docker), because each has a different way of setting it up.
1. If you just want to run / test in simple way (without setting ```virtual host```), you just can run ```php artisan serve``` on terminal / command line. It will serve you a link with port that you can use to access this Laravel project.

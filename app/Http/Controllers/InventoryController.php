<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Item;
use App\Models\Warehouse;
use App\Models\Mutation;

class InventoryController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth'); // Only authenticated users can access
        // Add more middleware for authorization if needed
    }

    public function index()
    {
        //
    }

    
    public function indexMutation($type)
    {
        // Implement logic for displaying index of route /inventory

        $mutations = Mutation::where("type",$type)->get();
        return view('inventory.mutations.index', ['mutations' => $mutations, 'type' => $type]);
        
    }

    public function createMutation()
    {
        $items = Item::all();
        $warehouses = Warehouse::all();

        return view('inventory.mutations.create', compact('items', 'warehouses'));
    }

    public function storeMutation(Request $request)
    {
        $validator = $this->validateMutationRequest($request);

        if ($validator->fails()) {
            return redirect()
                ->route('mutations.create')
                ->withErrors($validator)
                ->withInput();
        }

        $mutationType = $request->input('type');

        Mutation::create([
            'type' => $mutationType,
            'quantity' => $request->input('quantity'),
            'item_id' => $request->input('item_id'),
            'warehouse_id' => $request->input('warehouse_id'),
        ]);

        return redirect()->route('inventory.mutations.index', $mutationType)->with('success', ucfirst($mutationType) . ' mutation recorded successfully.');
    }
    

    public function trackInventory()
    {
        $inventory = Mutation::with(['item', 'warehouse'])->get();

        return view('inventory.track', compact('inventory'));
    }

    private function validateMutationRequest(Request $request)
    {
        return Validator::make($request->all(), [
            'type' => 'required|in:incoming,outgoing',
            'quantity' => 'required|numeric|min:1',
            'item_id' => 'required|exists:items,id',
            'warehouse_id' => 'required|exists:warehouses,id',
        ]);
    }
}

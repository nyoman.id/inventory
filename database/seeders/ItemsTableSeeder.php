<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Item;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $items = [
            ['name' => 'Komputer', 'description' => 'Perangkat elektronik yang digunakan untuk memproses data dan informasi. Komputer terdiri dari berbagai komponen, termasuk CPU, RAM, hard drive, dan kartu grafis. Komputer dapat digunakan untuk berbagai keperluan, termasuk mengetik, browsing internet, bermain game, dan menonton film.'],
            ['name' => 'Printer', 'description' => 'Perangkat yang digunakan untuk mencetak dokumen dan gambar. Printer terdiri dari berbagai jenis, termasuk inkjet printer, laser printer, dan thermal printer. Printer dapat digunakan untuk mencetak dokumen teks, gambar, foto, dan bahkan kartu nama. '],
            ['name' => 'Scanner', 'description' => 'Perangkat yang digunakan untuk memindai dokumen dan gambar. Scanner dapat digunakan untuk menyimpan dokumen dan gambar dalam format digital. Scanner dapat digunakan untuk berbagai keperluan, termasuk membuat salinan dokumen, menyimpan dokumen secara digital, dan membuat gambar digital. '],
            ['name' => 'Alat Tulis', 'description' => 'Alat yang digunakan untuk menulis dan menggambar. Alat tulis terdiri dari berbagai macam, termasuk pensil, pulpen, spidol, dan penghapus. Alat tulis dapat digunakan untuk berbagai keperluan, termasuk menulis catatan, menggambar, dan menghias. '],
            ['name' => 'Kursi', 'description' => 'Furnitur yang digunakan untuk duduk. Kursi dapat digunakan di berbagai tempat, termasuk di rumah, kantor, dan sekolah. Kursi dapat terbuat dari berbagai bahan, termasuk kayu, plastik, dan metal. '],
            ['name' => 'Meja', 'description' => 'Furnitur yang digunakan untuk meletakkan barang-barang. Meja dapat digunakan di berbagai tempat, termasuk di rumah, kantor, dan sekolah. Meja dapat terbuat dari berbagai bahan, termasuk kayu, plastik, dan metal. '],
            ['name' => 'Binder', 'description' => 'Alat yang digunakan untuk menyimpan dokumen. Binder terdiri dari berbagai jenis, termasuk binder plastik, binder metal, dan binder kain. Binder dapat digunakan untuk berbagai keperluan, termasuk menyimpan dokumen penting, membuat portfolio, dan membuat dokumen persembahan. '],
            ['name' => 'Kertas', 'description' => 'Bahan yang digunakan untuk menulis dan menggambar. Kertas dapat terbuat dari berbagai bahan, termasuk kayu, bambu, dan tanaman lainnya. Kertas dapat digunakan untuk berbagai keperluan, termasuk menulis catatan, menggambar, dan mencetak dokumen. ']
        ];
        foreach($items as $item){
            Item::create($item);
        }
    }
}
<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Warehouse;

class WarehousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $whse = [
            ['name' => 'Main Warehouse', 'location' => 'Main Building of Head Office'],
            ['name' => 'Branch Warehouse', 'location' => 'Main Branch Office'],
            ['name' => 'Logistik Warehouse', 'location' => 'Main Warehouse']
            
        ];
        foreach($whse as $wh){
            Warehouse::create($wh);
        }
    }
}

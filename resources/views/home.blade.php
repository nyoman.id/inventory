@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="d-flex justify-content-between">
                        <div class="col-6">
                            <a href="{{ route('masterdata.items.index') }}" class="me-3">Master Data Items</a>
                            <a href="{{ route('masterdata.warehouses.index') }}" class="me-3">Master Data Warehouse</a>
                        </div>
                        <div class="col-6">
                            <a href="{{ route('inventory.mutations.index', 'incoming') }}" class="me-3">Incoming Mutations</a>
                            <a href="{{ route('inventory.mutations.index', 'outgoing') }}" class="me-3">Outgoing Mutations</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!-- resources/views/inventory/create_mutations.blade.php -->

@extends('layouts.app')

@section('content')

    <div class="d-flex justify-content-between">
        <h1>Create Mutation</h1>
        <a href="{{ route('inventory.index').'/' }}" class="btn btn-warning mt-2 mb-1">Back to Inventory</a>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <form action="{{ route('inventory.mutations.store') }}" method="post">
        @csrf

        <div class="row">
            <div class="mb-3 col-6">
                <label for="type">Mutation Type:</label>
                <select name="type" id="type" class="form-control">
                    <option value="incoming">Incoming</option>
                    <option value="outgoing">Outgoing</option>
                </select>
            </div>
            <div class="mb-3 col-6">
                <label for="quantity">Quantity:</label>
                <input type="number" name="quantity" id="quantity" min="1" class="form-control" required>
            </div>
        </div>
        <div class="row">
            <div class="mb-3 col-6">
                <label for="item_id">Item:</label>
                <select name="item_id" id="item_id" class="form-control">
                    @foreach ($items as $item)
                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="mb-3 col-6">
                <label for="warehouse_id">Warehouse:</label>
                <select name="warehouse_id" id="warehouse_id" class="form-control">
                    @foreach ($warehouses as $warehouse)
                        <option value="{{ $warehouse->id }}">{{ $warehouse->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <button type="submit" class="btn btn-primary">Create Mutation</button>
    </form>
@endsection
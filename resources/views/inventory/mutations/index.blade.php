<!-- displaying incoming mutations -->

<!-- resources/views/inventory/incoming_mutations.blade.php -->

@extends('layouts.app')

@section('content')

    <div class="d-flex justify-content-between">
        <h1>{{ ucwords($type) }} Mutations</h1>
        <a href="{{ route('inventory.mutations.create') }}" class="btn btn-primary mt-2 mb-1">Create New</a>
    </div>

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <table>
        <thead>
            <tr>
                <th>Item</th>
                <th>Quantity</th>
                <th>Warehouse</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($mutations as $mutation)
                <tr>
                    <td>{{ $mutation->item->name }}</td>
                    <td>{{ $mutation->quantity }}</td>
                    <td>{{ $mutation->warehouse->name }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
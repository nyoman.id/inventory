<!-- resources/views/items/create.blade.php -->

@extends('layouts.app')

@section('content')

    <div class="d-flex justify-content-between">
        <h1>Create Item</h1>
        <a href="{{ route('masterdata.items.index') }}" class="btn btn-warning mt-2 mb-1">Back to List</a>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('masterdata.items.store') }}" method="post">
        @csrf

        <div class="mb-3">
            <label for="name">Item Name:</label>
            <input type="text" name="name" id="name" class="form-control" required>
        </div>
        <div class="mb-3">
            <label for="description">Description:</label>
            <textarea name="description" id="description" class="form-control" rows="3"></textarea>
        </div>

        <button type="submit" class="btn btn-primary">Create Item</button>

    </form>
@endsection

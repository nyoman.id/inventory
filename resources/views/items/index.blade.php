<!-- for listing items -->

<!-- resources/views/items/index.blade.php -->

@extends('layouts.app')

@section('content')

    <div class="d-flex justify-content-between">
        <h1>Items</h1>
        <a href="{{ route('masterdata.items.create') }}" class="btn btn-primary mt-2 mb-1">Create New</a>
    </div>

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($items as $item)
                <tr>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->description }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection


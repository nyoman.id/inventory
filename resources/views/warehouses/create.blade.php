<!-- resources/views/warehouses/create.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="d-flex justify-content-between">
        <h1>Create Warehouse</h1>
        <a href="{{ route('masterdata.warehouses.index') }}" class="btn btn-warning mt-2 mb-1">Back to List</a>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('masterdata.warehouses.store') }}" method="post">
        @csrf
        <div class="mb-3">
            <label for="name">Warehouse Name:</label>
            <input type="text" name="name" id="name" class="form-control" required>
        </div>

        <div class="mb-3">
            <label for="location">Location:</label>
            <input type="text" name="location" id="location" class="form-control" required>
        </div>

        <button type="submit" class="btn btn-primary">Create Warehouse</button>

    </form>
@endsection

<!-- listing warehouses -->

<!-- resources/views/warehouses.blade.php -->

@extends('layouts.app')

@section('content')
    <div class="d-flex justify-content-between">
        <h1>Warehouses</h1>
        <a href="{{ route('masterdata.warehouses.create') }}" class="btn btn-primary mt-2 mb-1">Create New</a>
    </div>

    @if (session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif

    <table>
        <thead>
            <tr>
                <th>Name</th>
                <th>Location</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($warehouses as $warehouse)
                <tr>
                    <td>{{ $warehouse->name }}</td>
                    <td>{{ $warehouse->location }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\InventoryController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\WarehouseController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


// Authentication routes (login, register, etc.)
Auth::routes();

Route::middleware(['auth'])->group(function () {

    Route::get('/home', [HomeController::class, 'index'])->name('home');

    // Inventory routes
    Route::group(['prefix'=>'/inventory','as'=>'inventory.'], function(){
        Route::get('/', [InventoryController::class, 'index'])->name('index');
        
        Route::group(['prefix'=>'/mutations','as'=>'mutations.'], function(){
            Route::get('/index/{type}', [InventoryController::class, 'indexMutation'])->name('index');
            Route::get('/create', [InventoryController::class, 'createMutation'])->name('create');
            Route::post('/store', [InventoryController::class, 'storeMutation'])->name('store');
        });

        // Route::get('/incoming-mutations', [InventoryController::class, 'incomingMutations'])->name('mutations.create');
        // Route::post('/incoming-mutations', [InventoryController::class, 'storeIncomingMutation'])->name('mutations.create');

        // Route::get('/outgoing-mutations', [InventoryController::class, 'outgoingMutations'])->name('mutations.create');
        // Route::post('/outgoing-mutations', [InventoryController::class, 'storeOutgoingMutation'])->name('mutations.create');
        
    });

    
    /**  Master Data routes **/

    Route::group(['prefix'=>'/masterdata','as'=>'masterdata.'], function(){

        // Items routes
        Route::group(['prefix'=>'/items','as'=>'items.'], function(){
            Route::get('/', [ItemController::class, 'index'])->name('index');
            Route::get('/create', [ItemController::class, 'create'])->name('create');
            Route::post('/store', [ItemController::class, 'store'])->name('store');
        });

        // Warehouses routes
        Route::group(['prefix'=>'/warehouses','as'=>'warehouses.'], function(){
            Route::get('/', [WarehouseController::class, 'index'])->name('index');
            Route::get('/create', [WarehouseController::class, 'create'])->name('create');
            Route::post('/store', [WarehouseController::class, 'store'])->name('store');
        });

    });

});


